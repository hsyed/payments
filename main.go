//go:generate protoc -I${GOPATH}/src -Iapi --gogo_out=plugins=grpc,paths=source_relative:. api.proto
package main

import (
	"github.com/spf13/cobra"
	"bitbucket.org/hsyed/payments/internal/server"
	"go.uber.org/fx"
	"context"
	"github.com/go-pg/pg"
	pg_migrations "github.com/go-pg/migrations"
	_ "bitbucket.org/hsyed/payments/internal/migrations"
	"go.uber.org/zap"
	"time"
)

var cmd *cobra.Command

func init() {
	var rt *fx.App

	var opts = &server.RuntimeOptions{}

	cmd = &cobra.Command{
		Use:   "payments",
		Short: "The payments microservice",
	}

	var (
		migrateCommand = &cobra.Command{
			Use:   "migrate",
			Short: "migrate the service",
			RunE: func(cmd *cobra.Command, args []string) error {
				var app *fx.App
				app = server.New(
					opts,
					server.DBModule,
					fx.Invoke(func(log *zap.Logger, db *pg.DB) error {
						if old, new, err := pg_migrations.Run(db); err != nil {
							return err
						} else if old == new {
							log.Sugar().Infof("no migrations needed at version %d", new)
						} else {
							log.Sugar().Infof("running migration from %d to %d", old, new)
						}
						return nil
					}))
				return app.Start(context.Background())
			},
		}

		launchCmd = &cobra.Command{
			Use:   "launch",
			Short: "launch the service",
			Run: func(cmd *cobra.Command, args []string) {
				rt = server.New(opts,
					server.DBModule,
					server.GrpcModule,
					server.GatewayModule,
					server.ServicesModule)

				// auto migration should not be allowed for production.
				if opts.AutoMigrate {
					var triedWithDelay bool
					// retry once for docker-compose startup races.
					for !triedWithDelay {
						if err := migrateCommand.RunE(cmd, args); err != nil {
							if triedWithDelay {
								panic(err)
							}
							time.Sleep(time.Duration(3 * time.Second))
							triedWithDelay = true
							continue
						}
						break
					}
				}

				if err := rt.Start(context.Background()); err != nil {
					panic(err)
				}
				select {
				case <-rt.Done():
				}
			},
		}
	)

	cmd.AddCommand(launchCmd, migrateCommand)

	cmd.PersistentFlags().StringVar(&opts.DB.Url, "db-url",
		"postgresql://postgres:postgres@localhost:5432/postgres?sslmode=disable", "The database url")

	launchCmd.PersistentFlags().StringVar(&opts.Grpc.Addr, "grpc-endpoint",
		"127.0.0.1:8081", "The GRPC addr")
	launchCmd.Flags().BoolVar(&opts.AutoMigrate, "auto-migrate", false, "should the server run a migration on startup")
	launchCmd.PersistentFlags().BoolVar(&opts.Gateway.OpenApi, "openapi", true,
		"enables or disable openapi documenation")
	launchCmd.PersistentFlags().StringVar(
		&opts.Gateway.Addr, "gateway-endpoint",
		"localhost:8080", "The GRPC gateway address")
}

func main() {
	if err := cmd.Execute(); err != nil {
		panic(err)
	}
}
