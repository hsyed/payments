# Payment Rest Service

This document outlines the design for the payment micro-service.

## Service Design

* The service will be written in Go.
* Proto will be used to encode the Rest IDL.
* The backend business logic will be implemented as a GRPC service.
* The GRPC service will be served as a Rest API via a rest-to-grpc bridge -- this bridge will be collocated in the same
  go process.
* The service will have generated documentation via swagger.

## Persistence and Data Model

I am taking the view that the example data provided is a format the service must support. I have also made the assumption 
that associative access by primary id is all that is required. To this end the following postgres schema is used for the 
data.

```sql
CREATE TABLE payment_records(
    uuid UUID NOT NULL PRIMARY KEY,
    data JSON NOT NULL
);
```
 
This choice means that there is no way to filter lists of payments in the database. This is not a block 
as by switching the type to `JSONB` and adding a `GIST` index on it we have the characteristics of MongoDB in Postgres.

Depending on the volatility of the data and the types of views need to be support a full relational model might make more 
sense.The example data contain mostly string values. The format won't be changed as this might be the case for 
interoperability with other systems.

## Rest Api Style

The api design is based on the [Google Api Design Guide](https://cloud.google.com/apis/design/).

The service will have a fairly standard Rest layout. The swagger documentation will give more detail. Standard rest 
concepts needed in production are out of scope for this task (pagination, filtering, field masking etc).

|  Verb  |         path          |                  purpose                   |
| :----- | :-------------------- | :----------------------------------------- |
| Get    | /api/v1/payments/{id} | Get a specific payment by id.              |
| Get    | /api/v1/payments      | List every payment in the system           |
| Patch  | /api/v1/payments      | Patch a payment, use the id from the body. |
| Delete | /api/v1/payments/{id} | Delete a specific payment                  |
| Post   | /api/v1/payments      | Create a payment                           |

 

## The Design of the Application

The application will:
 
* Use a DI framework to make wiring up of tests easy.
* Use a docker postgres service for testing.
* Use production-like logging patterns.
* Write tests against the GRPC layer, with a few tests against the rest layer for sanity.
* The application will have a `migrate` command.
 


