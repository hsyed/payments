{
  "swagger": "2.0",
  "info": {
    "title": "api/v1/payments/payments.proto",
    "description": "The payment microservice.",
    "version": "0.1"
  },
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/api/v1/payments": {
      "get": {
        "summary": "List all known payments.",
        "operationId": "List",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/paymentsListPaymentsResponse"
            }
          }
        },
        "tags": [
          "PaymentService"
        ]
      },
      "post": {
        "summary": "Create a new payment.",
        "operationId": "Create",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/protobufEmpty"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/paymentsPayment"
            }
          }
        ],
        "tags": [
          "PaymentService"
        ]
      }
    },
    "/api/v1/payments/{id}": {
      "get": {
        "summary": "Retrieve a single payment by id.",
        "operationId": "Get",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/paymentsPayment"
            }
          }
        },
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "PaymentService"
        ]
      },
      "delete": {
        "summary": "Delete an existing payment.",
        "operationId": "Delete",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/protobufEmpty"
            }
          }
        },
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "PaymentService"
        ]
      }
    },
    "/api/v1/payments/{payment.id}": {
      "patch": {
        "summary": "Update an existing payment.",
        "operationId": "Update",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/paymentsPayment"
            }
          }
        },
        "parameters": [
          {
            "name": "payment.id",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/paymentsPayment"
            }
          }
        ],
        "tags": [
          "PaymentService"
        ]
      }
    }
  },
  "definitions": {
    "ChargesInformationSenderCharge": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "string"
        },
        "currency": {
          "type": "string"
        }
      }
    },
    "paymentsBeneficiaryParty": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "account_name": {
          "type": "string"
        },
        "account_number": {
          "type": "string"
        },
        "account_number_code": {
          "type": "string"
        },
        "account_type": {
          "type": "integer",
          "format": "int32"
        },
        "address": {
          "type": "string"
        },
        "bank_id": {
          "type": "string"
        },
        "bank_id_code": {
          "type": "string"
        }
      },
      "description": "BeneficiaryParty represents the receiver of the Payment."
    },
    "paymentsChargesInformation": {
      "type": "object",
      "properties": {
        "bearer_code": {
          "type": "string"
        },
        "sender_charges": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ChargesInformationSenderCharge"
          }
        },
        "receiver_charges_amount": {
          "type": "string"
        },
        "receiver_charges_currency": {
          "type": "string"
        }
      },
      "description": "ChargesInformation misc information about the Payment."
    },
    "paymentsDebtorParty": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "account_name": {
          "type": "string"
        },
        "account_number": {
          "type": "string"
        },
        "account_number_code": {
          "type": "string"
        },
        "account_type": {
          "type": "string"
        },
        "address": {
          "type": "string"
        },
        "bank_id": {
          "type": "string"
        },
        "bank_id_code": {
          "type": "string"
        }
      },
      "description": "DebtorParty represents the source of the Payment."
    },
    "paymentsFx": {
      "type": "object",
      "properties": {
        "contract_reference": {
          "type": "string"
        },
        "exchange_rate": {
          "type": "string"
        },
        "original_amount": {
          "type": "string"
        },
        "original_currency": {
          "type": "string"
        }
      },
      "description": "Fx are the foreign exchanges rates at the time the payment was recorded."
    },
    "paymentsListPaymentsResponse": {
      "type": "object",
      "properties": {
        "payments": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/paymentsPayment"
          }
        }
      }
    },
    "paymentsPayment": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string"
        },
        "id": {
          "type": "string"
        },
        "version": {
          "type": "integer",
          "format": "int32"
        },
        "organization_id": {
          "type": "string"
        },
        "attributes": {
          "$ref": "#/definitions/paymentsPaymentAttributes"
        }
      },
      "title": "A single payment transaction"
    },
    "paymentsPaymentAttributes": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "string"
        },
        "currency": {
          "type": "string"
        },
        "end_to_end_reference": {
          "type": "string"
        },
        "numeric_reference": {
          "type": "string"
        },
        "payment_id": {
          "type": "string"
        },
        "payment_purpose": {
          "type": "string"
        },
        "payment_scheme": {
          "type": "string"
        },
        "scheme_payment_type": {
          "type": "string"
        },
        "scheme_payment_sub_type": {
          "type": "string"
        },
        "benificiary_party": {
          "$ref": "#/definitions/paymentsBeneficiaryParty"
        },
        "debtor_party": {
          "$ref": "#/definitions/paymentsDebtorParty"
        },
        "charges_information": {
          "$ref": "#/definitions/paymentsChargesInformation"
        },
        "sponsor_party": {
          "$ref": "#/definitions/paymentsSponsorParty"
        },
        "fx": {
          "$ref": "#/definitions/paymentsFx"
        }
      },
      "title": "PaymentAttributes are the attributes of a payment"
    },
    "paymentsSponsorParty": {
      "type": "object",
      "properties": {
        "account_number": {
          "type": "string"
        },
        "bank_id": {
          "type": "string"
        },
        "bank_id_code": {
          "type": "string"
        }
      }
    },
    "protobufEmpty": {
      "type": "object",
      "description": "service Foo {\n      rpc Bar(google.protobuf.Empty) returns (google.protobuf.Empty);\n    }\n\nThe JSON representation for `Empty` is empty JSON object `{}`.",
      "title": "A generic empty message that you can re-use to avoid defining duplicated\nempty messages in your APIs. A typical example is to use it as the request\nor the response type of an API method. For instance:"
    }
  },
  "externalDocs": {
    "description": "Payments service",
    "url": "https://bitbucket.org/hsyed/payments"
  }
}
