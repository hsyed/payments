package migrations

import (
	"github.com/go-pg/migrations"
	"log"
)


func registerMigration(mig Migration) {
	// I do not trust this migration package.
	migrations.Register(
		func(db migrations.DB) error {
			log.Printf("running migration script: \n%s", mig.Up)
			_, err := db.Exec(mig.Up)
			return err
		}, func(db migrations.DB) error {
			log.Printf("running migration dowm script: \n%s", mig.Down)
			_, err := db.Exec(mig.Down)
			return err
		},
	)
}

type Migration struct {
	Up string
	Down string
}

/*language=PostgreSQL*/
var upScript = `
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE payment_records(
    uuid UUID NOT NULL PRIMARY KEY,
    data JSON NOT NULL
);
`

/*language=PostgreSQL*/
var downScript = `
DROP TABLE IF EXISTS payment_records;
DROP EXTENSION IF EXISTS "uuid-ossp";
`

func init() {
	registerMigration(Migration{upScript, downScript})
}
