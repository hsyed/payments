package v1

import (
	"github.com/gogo/protobuf/types"
	"golang.org/x/net/context"
	"github.com/go-pg/pg"
	"go.uber.org/zap"
	"github.com/gogo/protobuf/jsonpb"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/codes"
	. "bitbucket.org/hsyed/payments/api/v1/payments"
	"go.uber.org/fx"
)

type PaymentsService struct {
	fx.In

	Db  *pg.DB
	Log *zap.SugaredLogger
}

// Dao struct for storing payments the payment in Postgres.
type paymentRecord struct {
	Uuid string
	Data string
}

func (ps *PaymentsService) List(context context.Context, request *ListPaymentsRequest) (*ListPaymentsResponse, error) {
	response := &ListPaymentsResponse{}
	var envelopes []paymentRecord
	if err := ps.Db.WithContext(context).Model(&paymentRecord{}).Select(&envelopes); err != nil {
		return nil, ps.renderUnknownPersistenceError(err)
	} else {
		for _, v := range envelopes {
			if p, err := ps.recordToPayment(&v); err != nil {
				return nil, ps.renderUnknownPersistenceError(err)
			} else {
				response.Payments = append(response.Payments, p)
			}

		}
		return response, nil
	}
}

func (ps *PaymentsService) Create(ctx context.Context, req *CreatePaymentRequest) (*types.Empty, error) {
	if record, err := ps.recordFromPayment(req.Payment); err != nil {
		return nil, err
	} else if _, err := ps.Db.WithContext(ctx).Model(record).Insert(); err != nil {
		return nil, ps.tryRenderKnownPersistenceError(err)
	}
	return &types.Empty{}, nil
}

func (ps *PaymentsService) Get(ctx context.Context, req *GetPaymentsRequest) (*Payment, error) {
	rec := &paymentRecord{}
	if err := ps.Db.WithContext(ctx).Model(&paymentRecord{Uuid: req.Id}).WherePK().Select(rec); err != nil {
		if err == pg.ErrNoRows {
			return nil, doesNotExist
		} else {
			return nil, ps.renderUnknownPersistenceError(err)
		}
	}
	return ps.recordToPayment(rec)
}

func (ps *PaymentsService) Update(ctx context.Context, req *UpdatePaymentsRequest) (*Payment, error) {
	if record, err := ps.recordFromPayment(req.Payment); err != nil {
		return nil, err
	} else if res, err := ps.Db.WithContext(ctx).Model(record).WherePK().Update(); err != nil {
		return nil, ps.renderUnknownPersistenceError(err)
	} else if res.RowsAffected() == 0 {
		return nil, doesNotExist
	} else {
		return req.Payment, nil
	}
}

func (ps *PaymentsService) Delete(ctx context.Context, req *DeletePaymentsRequest) (*types.Empty, error) {
	query := ps.Db.WithContext(ctx).Model(&paymentRecord{Uuid: req.Id}).WherePK()
	if result, err := query.Delete(); err != nil {
		return nil, ps.renderUnknownPersistenceError(err)
	} else if result.RowsAffected() == 0 {
		return nil, doesNotExist
	} else {
		return &types.Empty{}, nil
	}
}

var (
	genericPersistenceError = status.Error(codes.Internal, "persistence layer error")
	doesNotExist            = status.Error(codes.NotFound, "entity did not exist")
	alreadyExists           = status.Error(codes.AlreadyExists, "entity already exists")
)

// The logging here is a bit more involved. In a real codebase this is the standard to how I would log
// postgres errors when designing a proper model.
func (ps *PaymentsService) tryRenderKnownPersistenceError(err error) error {
	if pgError, ok := err.(pg.Error); !ok {
		return ps.renderUnknownPersistenceError(err)
	} else {
		code := pgError.Field('C')
		switch code {
		case "23505":
			ps.Log.Info(
				"crud error",
				zap.String("table", pgError.Field('t')),
				zap.String("message", pgError.Field('M')),
				zap.String("constraint", pgError.Field('n')),
			)
			return alreadyExists
		default:
			ps.Log.Info("generic postgres error",
				zap.String("status_code", code),
				zap.String("message", pgError.Field('M')),
				zap.Bool("integrity_violation", pgError.IntegrityViolation()),
			)
			return genericPersistenceError
		}
	}
}

// Generate and log an unknown persistence error, it could be nil in which case it's just returned.
func (ps *PaymentsService) renderUnknownPersistenceError(err error) (error) {
	if err == nil {
		return nil
	}
	ps.Log.Errorf("persistence error: %s", err)
	return genericPersistenceError
}

// Render a paymentRecord from a Payment object
func (ps *PaymentsService) recordFromPayment(payment *Payment) (record *paymentRecord, err error) {
	record = &paymentRecord{Uuid: payment.Id}
	if record.Data, err = (&jsonpb.Marshaler{}).MarshalToString(payment); err != nil {
		ps.Log.Errorf("could not marshall record to json: %s", err)
		err = status.Error(codes.Internal, "internal server error")
	}
	return
}

// Convert a a record back out to a Payment.
func (ps *PaymentsService) recordToPayment(record *paymentRecord) (payment *Payment, err error) {
	payment = &Payment{}
	if err := jsonpb.UnmarshalString(record.Data, payment); err != nil {
		ps.Log.Errorf("error unmarshalling json payload: %s", err)
		return nil, genericPersistenceError
	} else {
		return payment, nil
	}
}
