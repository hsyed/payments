package v1_test

import (
	"bitbucket.org/hsyed/payments/internal/server"
	"github.com/franela/goblin"
	. "github.com/onsi/gomega"
	"go.uber.org/fx"
	"context"
	"github.com/go-pg/pg"
	_ "bitbucket.org/hsyed/payments/internal/migrations"
	"github.com/go-pg/migrations"
	"strings"
	"go.uber.org/zap"
	"bitbucket.org/hsyed/payments/api/v1/payments"
	"testing"
	"google.golang.org/grpc/codes"
	"github.com/google/uuid"
	"github.com/gogo/protobuf/proto"
	"google.golang.org/grpc/status"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"net/http"
	"github.com/gogo/protobuf/jsonpb"
)

func cleanAndMigrate(log *zap.SugaredLogger, db *pg.DB) error {
	if !strings.HasPrefix(db.Options().Addr, "localhost") {
		log.Fatalf("test migrations can only be run against a db running on localhost")
	}
	return db.RunInTransaction(func(tx *pg.Tx) error {
		if _, _, err := migrations.Run(tx, "reset"); err != nil {
			return err
		} else if _, err := tx.Exec("DROP TABLE IF EXISTS gopg_migrations;"); err != nil {
			return err
		} else {
			_, _, err := migrations.Run(tx)
			return err
		}
	})
}

func setupServerSuite(targets ...interface{}) *fx.App {

	var testRtOpts = &server.RuntimeOptions{}
	testRtOpts.DB.Url = "postgresql://postgres:postgres@localhost:5432/postgres?sslmode=disable"
	testRtOpts.Grpc.Addr = "127.0.0.1:8081"
	testRtOpts.Gateway.Addr = "localhost:8080"
	testRtOpts.Gateway.OpenApi = false

	app := server.New(
		testRtOpts,
		server.DBModule,
		fx.Invoke(cleanAndMigrate),
		server.GrpcModule,
		server.GatewayModule,
		server.ServicesModule,
		fx.Populate(targets...),

	)
	if err := app.Start(context.Background()); err != nil {
		panic(err)
	}
	return app
}

func Suite(t *testing.T) *goblin.G {
	g := goblin.Goblin(t)
	RegisterFailHandler(func(m string, _ ...int) { g.Fail(m) })
	return g
}


func TestPaymentsGrpc(t *testing.T) {
	var client payments.PaymentServiceClient
	g := Suite(t)
	app := setupServerSuite(&client)
	defer app.Stop(context.Background())

	g.Describe("The payments service", func() {
		firstPayment := &payments.Payment{
			Id:             uuid.New().String(),
			Type:           "something",
			OrganizationId: "hassan",
			Version:        0,
			Attributes: &payments.PaymentAttributes{

			},
		}

		g.Describe("The create endpoint", func() {
			g.It("should allow the creation of payments", func() {
				_, err := client.Create(context.Background(), &payments.CreatePaymentRequest{Payment: firstPayment})
				Ω(err).ShouldNot(HaveOccurred())
			})

			g.It("should return an appropriate GRPC code if trying to create a payment with the same Id", func() {
				_, err := client.Create(context.Background(), &payments.CreatePaymentRequest{Payment: firstPayment})
				expectGrpcStatusError(err, codes.AlreadyExists)
			})
		})
		g.Describe("The get endpoint", func() {
			g.It("should retrieve a known payment by id", func() {
				res, err := client.Get(context.Background(), &payments.GetPaymentsRequest{Id: firstPayment.Id})
				Ω(err).ShouldNot(HaveOccurred())
				Ω(res.Id).Should(Equal(firstPayment.Id))
			})
			g.It("should return an appropriate error when retrieving non existing payments", func() {
				_, err := client.Get(context.Background(), &payments.GetPaymentsRequest{Id: uuid.New().String()})
				expectGrpcStatusError(err, codes.NotFound)
			})
		})
		g.Describe("The update endpoint", func() {
			g.It("should allow updating a known id", func() {
				updatedPayment := proto.Clone(firstPayment).(*payments.Payment)
				updatedPayment.OrganizationId = "hassan-b"
				res, err := client.Update(context.Background(), &payments.UpdatePaymentsRequest{
					Payment: updatedPayment,
				})
				Ω(err).ShouldNot(HaveOccurred())
				Ω(res.Id).Should(Equal(firstPayment.Id))
				Ω(res.OrganizationId).Should(Equal("hassan-b"))
			})

			g.It("should return an error when updating an unknown id", func() {
				updatedPayment := proto.Clone(firstPayment).(*payments.Payment)
				updatedPayment.Id = uuid.New().String()
				_, err := client.Update(context.Background(), &payments.UpdatePaymentsRequest{
					Payment: updatedPayment,
				})
				expectGrpcStatusError(err, codes.NotFound)
			})
		})
		g.Describe("The delete endpoint", func() {
			g.It("should allow deleting existing entries", func() {
				_, err := client.Delete(context.Background(), &payments.DeletePaymentsRequest{Id: firstPayment.Id})
				Ω(err).ShouldNot(HaveOccurred())

				_, err = client.Get(context.Background(), &payments.GetPaymentsRequest{Id: firstPayment.Id})
				expectGrpcStatusError(err, codes.NotFound)
			})

			g.It("should return an appropriate code when deleting existing entries", func() {
				_, err := client.Delete(context.Background(), &payments.DeletePaymentsRequest{Id: firstPayment.Id})
				expectGrpcStatusError(err, codes.NotFound)
			})
		})
		g.It("should have an endpoint for listing entries", func() {
			_, err := client.Create(context.Background(), &payments.CreatePaymentRequest{Payment: firstPayment})
			Ω(err).ShouldNot(HaveOccurred())
			firstPayment.Id = uuid.New().String()

			_, err = client.Create(context.Background(), &payments.CreatePaymentRequest{Payment: firstPayment})
			Ω(err).ShouldNot(HaveOccurred())

			res, err := client.List(context.Background(), &payments.ListPaymentsRequest{})
			Ω(res.Payments).ShouldNot(BeEmpty())
		})
	})
}


func TestServerRest(t *testing.T) {
	fixtureEntries := paymentFixtureEntries()
	g := Suite(t)
	app := setupServerSuite()
	defer app.Stop(context.Background())
	g.Describe("The Grpc Rest Gateway", func() {
		client := &http.Client{}

		g.Describe("/api/v1/payments should", func() {
			g.It("should support POST and allow us to upload some fixtures", func() {
				marshaller := jsonpb.Marshaler{}
				for _, v := range fixtureEntries {
					entry1, _ := marshaller.MarshalToString(&v)
					reader := strings.NewReader(entry1)
					_, err := http.Post("http://localhost:8080/api/v1/payments", "application/json", reader)
					Ω(err).ShouldNot(HaveOccurred())
				}
			})
			g.It("should have a GET that lists the payments", func() {
				if req, err := http.NewRequest(http.MethodGet, "http://localhost:8080/api/v1/payments", nil); err != nil {
					Ω(err).ShouldNot(HaveOccurred())
				} else if res, err := client.Do(req); err != nil {
					Ω(err).ShouldNot(HaveOccurred())
				} else {
					defer res.Body.Close()
					payments := &payments.ListPaymentsResponse{}
					if body, err := ioutil.ReadAll(res.Body); err != nil {
						Ω(err).ShouldNot(HaveOccurred())
					} else if err := json.Unmarshal(body, &payments); err != nil {
						Ω(err).ShouldNot(HaveOccurred())
					} else if len(payments.Payments) == 0 {
						g.Fail("payments has no items")
					}
				}
			})
			getPayment := func(id string, expectCode int) (*payments.Payment, error) {
				payment := &payments.Payment{}
				url := fmt.Sprintf("http://localhost:8080/api/v1/payments/%s", id)
				if req, err := http.Get(url); err != nil {
					return nil, err
				} else {
					defer req.Body.Close()
					if expectCode != 0 {
						Ω(req.StatusCode).Should(Equal(expectCode))
						return nil, nil
					}
					if body, err := ioutil.ReadAll(req.Body); err != nil {
						return nil, err
					} else if err := json.Unmarshal(body, &payment); err != nil {
						return nil, err
					} else {
						return payment, nil
					}
				}
				return nil, nil
			}

			deletePayment := func(id string) {
				url := fmt.Sprintf("http://localhost:8080/api/v1/payments/%s", id)
				req, _ := http.NewRequest(http.MethodDelete, url, nil)
				if res, err := http.DefaultClient.Do(req); err != nil {
					Ω(err).ShouldNot(HaveOccurred())
				} else {
					res.Body.Close()
				}
			}
			g.It("GET on /api/v1/payments/{id}", func() {
				pm, err := getPayment(fixtureEntries[0].Id, 0)
				Ω(err).ShouldNot(HaveOccurred())
				Ω(pm.Id).Should(Equal(fixtureEntries[0].Id))
			})
			g.It("DELETE on /api/v1/payments/{id}", func() {
				deletePayment(fixtureEntries[0].Id)
				getPayment(fixtureEntries[0].Id, http.StatusNotFound)
			})
		})
	})
}

func expectGrpcStatusError(err error, code codes.Code) {
	Ω(err).Should(HaveOccurred())
	status, ok := status.FromError(err)
	Ω(ok).Should(BeTrue())
	Ω(status.Code()).Should(Equal(code))
}
