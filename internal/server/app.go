package server

import (
	"go.uber.org/fx"
	"github.com/go-pg/pg"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	"github.com/gogo/gateway"
	"context"
	"net"
	"fmt"
	"net/http"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"mime"
	"github.com/rakyll/statik/fs"
	_ "bitbucket.org/hsyed/payments/statik"
	"google.golang.org/grpc/connectivity"
	"time"
	"strings"
)

// Service Configuration type.
type (
	PrimaryMux *http.ServeMux
	GatewayMux *runtime.ServeMux
	PrimaryClientConn *grpc.ClientConn

	Runtime struct {
		*fx.App

		options *RuntimeOptions
		ctx     context.Context
		cancel  context.CancelFunc

		// The database that services should use.
		db *pg.DB
		// The logger that services should use.
		log *zap.Logger

		grpc *grpc.Server
		// if not null it is enabled.
		httpServer *http.Server
		clientConn *grpc.ClientConn
	}

	GrpcOptions struct {
		Addr string
	}

	GatewayOptions struct {
		// if this is not empty it is enabled.
		Addr    string
		OpenApi bool
	}

	DBOptions struct {
		// The db conn url.
		Url string
		// Function to validate the connection
		TestConnection func(db *pg.DB) error
		// Further customize the db options
		InitOptions func(options *pg.Options)
	}

	RuntimeOptions struct {
		// If true sets up production logging.
		IsProduction bool
		AutoMigrate  bool

		Grpc    GrpcOptions
		Gateway GatewayOptions
		DB      DBOptions
	}
)

// preprocess RuntimeOptions before starting the DI wireup.
func initRuntimeOptions(o *RuntimeOptions) fx.Option {
	return fx.Options(
		fx.Provide(
			//func() *RuntimeOptions { return options },

			func() (log *zap.Logger, sugared *zap.SugaredLogger, err error) {
				if o.IsProduction {
					log, err = zap.NewProduction()
				} else {
					log, err = zap.NewDevelopment()
				}
				return log, log.Sugar(), err
			},


			func(log *zap.Logger) (*DBOptions, *GrpcOptions, *GatewayOptions, error) {
				if o.DB.TestConnection == nil {
					o.DB.TestConnection = func(db *pg.DB) error {
						var dummy int
						if _, err := db.QueryOne(&dummy, "SELECT 1;"); err != nil {
							return err
						} else {
							return nil
						}
					}
				}

				if o.DB.InitOptions == nil {
					o.DB.InitOptions = func(options *pg.Options) {
						options.PoolSize = 8
					}
				}

				return &o.DB, &o.Grpc, &o.Gateway, nil
			},


		),

	)
}

// This module wires up the database but it does not verify the database is reachable.
var DBModule = fx.Options(
	fx.Provide(
		func(lifecycle fx.Lifecycle, log *zap.SugaredLogger, ro *DBOptions) (*pg.DB, error) {
			var err error
			var o *pg.Options
			if o, err = pg.ParseURL(ro.Url); err != nil {
				log.Errorw("could not init db", "url", ro.Url, "error", err)
				return nil, err
			} else {
				ro.InitOptions(o)
			}
			db := pg.Connect(o)
			return db, nil
		},
	),
)

// This module wires up the GRPC process as well as validating the database is reachable.
var GrpcModule = fx.Options(
	fx.Provide(
		func(log *zap.Logger, db *pg.DB, lifecycle fx.Lifecycle, opt *GrpcOptions) (*grpc.Server, error) {
			server := grpc.NewServer(
				grpc.UnaryInterceptor(grpc_zap.UnaryServerInterceptor(log)),
				grpc.StreamInterceptor(grpc_zap.StreamServerInterceptor(log)),
			)

			var err error
			var grpcListener net.Listener
			if grpcListener, err = net.Listen("tcp", opt.Addr); err != nil {
				log.Sugar().Errorf("could not open grpc sock: %s", err)
				return nil, err
			}

			lifecycle.Append(
				fx.Hook{
					OnStart: func(context context.Context) (err error) {

						var dummy int
						if _, err := db.QueryOne(&dummy, "SELECT 1;"); err != nil {
							log.Sugar().Errorw("could not validate connection to db", "error", err)
							return err
						} else {
							log.Sugar().Infow("started postgres connection",
								"database", db.Options().Database,
								"user", db.Options().User,
								"pool_size", db.Options().PoolSize)
						}

						log.Sugar().Info("grpc service running on ", resolve(opt.Addr))
						go func() {
							if err := server.Serve(grpcListener); err != nil {
								log.Sugar().Fatal(err)
							}
						}()
						return nil
					},
					OnStop: func(context context.Context) error {
						server.GracefulStop()
						db.Close()
						return nil
					},
				},
			)
			return server, nil
		},

		func(log *zap.SugaredLogger, server *grpc.Server, opt *GrpcOptions, lifecycle fx.Lifecycle) (con PrimaryClientConn, err error) {
			dialAddr := fmt.Sprintf("passthrough://127.0.0.1/%s", opt.Addr)

			if con, err = grpc.Dial(dialAddr, grpc.WithInsecure()); err != nil {
				log.Errorf("failed to dial server: %s", err)
				return nil, err
			}

			lifecycle.Append(fx.Hook{
				OnStart: func(ctx context.Context) error {
					withTimeout, cancel := context.WithTimeout(ctx, time.Duration(10*time.Second))
					defer cancel()
					co := (*grpc.ClientConn)(con)
					for last := co.GetState(); last != connectivity.Ready; last = co.GetState() {
						if ! (*grpc.ClientConn)(con).WaitForStateChange(withTimeout, connectivity.Ready) {
							log.Fatalf("could not dial grpc server")
						}
					}
					if co.GetState() != connectivity.Ready {
						log.Fatalf("could not dial grpc server")
					}
					return nil
				},
			})
			return con, err
		},
	),
)

// This module wired up the grpc-rest-gateway
var GatewayModule = fx.Options(
	fx.Provide(
		func(opt *GatewayOptions, log *zap.SugaredLogger, lifecycle fx.Lifecycle) (PrimaryMux, GatewayMux, error) {
			primaryMux := http.NewServeMux()
			jsonpb := &gateway.JSONPb{
				EmitDefaults: true,
				Indent:       "  ",
				OrigName:     true,
			}

			gwMux := runtime.NewServeMux(
				runtime.WithMarshalerOption(runtime.MIMEWildcard, jsonpb),
				runtime.WithProtoErrorHandler(runtime.DefaultHTTPProtoErrorHandler),
			)

			if opt.OpenApi {
				mime.AddExtensionType(".svg", "image/svg+xml")
				if statikFS, err := fs.New(); err != nil {
					log.Errorf("could not setup openApi: %s", err)
					return nil, nil, err
				} else {
					// Expose files in static on <host>/openapi-ui
					fileServer := http.FileServer(statikFS)
					prefix := "/openapi-ui/"
					primaryMux.Handle(prefix, http.StripPrefix(prefix, fileServer))
					log.Infof("OpenApi documentation available at http://%s%s", resolve(opt.Addr), prefix)
				}
			} else {
				log.Info("openapi feature not enabled")
			}

			primaryMux.Handle("/", gwMux)

			if opt.Addr != "" {
				server := &http.Server{
					Addr:    opt.Addr,
					Handler: primaryMux,
				}
				lifecycle.Append(
					fx.Hook{
						OnStart: func(ctx context.Context) error {
							if lis, err := net.Listen("tcp", server.Addr); err != nil {
								log.Fatalf("could not start gateway: %s", err)
								return err
							} else {
								go func() {
									if err := server.Serve(lis); err != http.ErrServerClosed {
										panic(err)
									}
								}()
							}
							log.Infof("grpc gateway running at http://%s", resolve(opt.Addr))
							return nil
						},
						OnStop: func(ctx context.Context) error {
							return server.Close()
						},
					},
				)
			} else {
				log.Info("grpc gateway not enabled")
			}

			return primaryMux, gwMux, nil
		},
	),
)

// utility to resolve the addr string
func resolve(addr string) string {
	parts := strings.Split(addr, ":")
	if len(parts) != 2 {
		panic("invalid addr, must be colon separated")
	}
	host := "localhost"
	if trimmed := strings.TrimSpace(parts[0]); len(trimmed) != 0 {
		return addr
	} else {
		return fmt.Sprintf("%s:%s", host, parts[1])
	}

}

func New(options *RuntimeOptions, fxOptions ... fx.Option) *fx.App {
	return fx.New(
		append([]fx.Option{
			initRuntimeOptions(options),
			fx.NopLogger,
		}, fxOptions...)...
	)

}
