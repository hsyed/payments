package server

import (
	"go.uber.org/fx"
	"google.golang.org/grpc"
	"bitbucket.org/hsyed/payments/internal/svc/api/v1"
	"bitbucket.org/hsyed/payments/api/v1/payments"
	"context"
)

type serviceRegParams struct {
	fx.In

	Server  *grpc.Server
	Gateway GatewayMux
	Conn    PrimaryClientConn

	PaymentsService v1.PaymentsService
}

type serviceClients struct {
	fx.Out

	PaymentsClient payments.PaymentServiceClient
}

var ServicesModule = fx.Options(

	fx.Invoke(
		func(p serviceRegParams) {
			payments.RegisterPaymentServiceServer(p.Server, &p.PaymentsService)
			payments.RegisterPaymentServiceHandler(context.Background(), p.Gateway, p.Conn)
		},
	),

	fx.Provide(
		func(conn PrimaryClientConn) serviceClients {
			return serviceClients{
				PaymentsClient: payments.NewPaymentServiceClient(conn),
			}
		},
	),

)
