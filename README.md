# Usage

The package path for the code is `bitbucket.org/hsyed/payments`.

`docker-compose` is required to launch postgres and build the app. Postgres can be started manually if prefered. 

```bash
# This will use docker-compose to build the image (the go binary is placed in an empty container).
make build
# This will run the tests against a db created in compose. The db will be launched detatched.
make test
# This will start the service in the foreground, which should allow you to view the swagger docs.
make launch
# this will tear down everything.
make down 
```

## Directories

* **api** This contains the proto files and generated code.
* **public** This contains the swagger ui and generated swagger JSON API file.
* **static** This contains a go code generated package with the contents of the public folder encoded in binary form. 
  This is used at runtime to serve `swagger-ui`. 
* **internal/server** This package contains the go logic for wiring up a server.
* **internal/svc** The implementations of the payment service is found in here.
* **internal/migrations** The migrations are found here.


## Note:

Running `make launch` without having run a `make test` will result in a server without data. If the tests are run once 
before `make launch` there should be some data left behind to work with. 
